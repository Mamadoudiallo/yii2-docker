Yii2 Docker
-----------

This is a basic docker image for developing Yii2 applications. It's features include:

- PHP 7.0
- Nginx 

The default nginx config shipped with this image will use `/var/www/html/frontend/web` as it's document root.

Volumes included are:

- `/var/www/html`
- `/home/dev/src`

Development
-----------

After logging into the image run `su - dev` to switch to the dev account.   